
# build target specs
CC = g++
CFLAGS =-std=c++0x -Wall -Iinclude -Llib
SOURCES=src/Commands/*.cpp src/*.cpp  main.cpp
LIBS = -lsqlite3 -largirc -lconfig++ -ldl
EXEC = hagobot

all: $(EXEC)

argirc:
	@make -C ./ARGIrc
	@make -C ./ARGIrc copy
argircdebug:
	@make -C ./ARGIrc debug
	@make -C ./ARGIrc copy

$(EXEC): argirc
	@echo -n 'Linking $(EXEC)... '
	$(CC) -o $(EXEC) $(CFLAGS) -O3 $(SOURCES) $(LIBS)  
	@echo Done.

debugbin: argircdebug
	@echo -n 'Linking $(EXEC)... '
	$(CC) -o $(EXEC) $(CFLAGS) -O0 $(SOURCES) $(LIBS)  
	@echo Done.


run: 
	export LD_LIBRARY_PATH=./lib; ./$(EXEC) 

daemon:
	export LD_LIBRARY_PATH=./lib; ./$(EXEC) -d
debug:
	export LD_LIBRARY_PATH=./lib; gdb ./$(EXEC)

clean:
	@echo Removing all... 
	@rm -f $(EXEC) *.o
	@make -C ./ARGIrc clean
	@echo Done.

clear:
	@echo Removing all... 
	@rm -f $(EXEC) *.o
	@make -C ./ARGIrc clean
	@echo Done.
