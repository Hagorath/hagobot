/**********************************************************************
* Copyright (C) 2006 by Andre                                         *
*                                                                     *
* This library is free software; you can redistribute it and/or       *
* modify it under the terms of the GNU Lesser General Public          *
* License as published by the Free Software Foundation; either        *
* version 2.1 of the License, or (at your option) any later version.  *
*                                                                     *
* This library is distributed in the hope that it will be useful,     *
* but WITHOUT ANY WARRANTY; without even the implied warranty of      *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   *
* Lesser General Public License for more details.                     *
*                                                                     *
* You should have received a copy of the GNU Lesser General Public    *
* License along with this library; if not, write to the Free Software *
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,          *
* MA  02110-1301  USA                                                 *
**********************************************************************/

#include <iostream>
#include <ctime>
#include <cstdio>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <cstring>
#include "Irc.h"

using namespace std;
using namespace ARGIrc;

// constructor
Irc::Irc()
{
    // default to no debug output
    debugOutputEnabled = false;
}

// destructor
Irc::~Irc()
{
}

// run method (connects to server and starts parsing events)
bool Irc::start ( const string& serverHostname, const string& serverPassword, const int serverPort, const string& nickname, const string& userName, const string& realName )
{
    struct hostent* he;
    struct sockaddr_in remoteAddr;

    // initialize vars for connection
    this->nickname = nickname;
    runFlag = true;

    // get host
    debugMessage ( "Getting host by name..." );
    if ( ( he = gethostbyname ( serverHostname.c_str() ) ) == NULL ) {
        debugMessage ( "Could not get host!" );
        return false;
    }
    debugMessage ( "Got host!" );

    // create socket
    debugMessage ( "Creating socket..." );
    if ( ( socketFd = socket ( AF_INET, SOCK_STREAM, 0 ) ) < 0 ) {
        debugMessage ( "Could not create socket!" );
        return false;
    }
    debugMessage ( "Socket created!" );

    // set up remote addr struct
    remoteAddr.sin_family = AF_INET;
    remoteAddr.sin_port = htons ( serverPort );
    remoteAddr.sin_addr = * ( ( struct in_addr * ) he->h_addr );
    memset ( & ( remoteAddr.sin_zero ), '\0', 8 );

    // connect
    debugMessage ( "Connection..." );
    if ( connect ( socketFd, ( struct sockaddr * ) &remoteAddr, sizeof ( struct sockaddr ) ) < 0 ) {
        debugMessage ( "Could not connect!" );
        return false;
    }
    debugMessage ( "Connected!" );

    // log in
    sendLine ( "PASS " + serverPassword );
    sendLine ( "USER " + userName + " 0 * :" + realName );
    sendLine ( "NICK " + nickname );

    // begin processing server messages
    while ( runFlag ) {
        string line;

        // read a line
        if ( !readLine ( line ) ) break;

        // call raw line handler
        onRawLine ( line );

        // check for PING
        if ( line.find ( "PING" ) == 0 ) {
            // build response and send to on ping handler
            string response = "PONG ";
            response += line.substr ( 5 );
            onPing ( response );
            // check for error
        } else if ( line.find ( "ERROR" ) == 0 ) {
            // error occured, we probably disconnected
            debugMessage ( "Disconnected!" );
            onDisconnect();
            // deal with other commands
        } else {
            // get command and message
            string commandInfo;
            string message;
            int splitterIndex = line.find ( ":", 1 );
            commandInfo = line.substr ( 0, splitterIndex - 1 );
            message = line.substr ( splitterIndex + 1 );

            // check if we have 442 or 376 (end of motd or no motd)
            if ( ( commandInfo.find ( "442" ) != string::npos ) || ( commandInfo.find ( "376" ) != string::npos ) ) {
                // fire on connect method and display debug info
                debugMessage ( "Connected and logged in!" );
                onConnect();
            }

            // check if we have an @ in command (good sign that we might be interested in the line)
            if ( commandInfo.find ( "@" ) != string::npos ) {
                // get sender info
                int senderSpaceIndex = commandInfo.find ( " " );
                string senderInfo = commandInfo.substr ( 0, senderSpaceIndex );

                // get nickname from sender info
                int nicknameIndex = senderInfo.find ( "!" );
                string senderNickname = senderInfo.substr ( 1, nicknameIndex - 1 );

                // get username from sender info
                int usernameIndex = senderInfo.find ( "@" );
                string senderUsername = senderInfo.substr ( nicknameIndex + 1, usernameIndex - nicknameIndex - 1 );

                // get hostname from sender info
                string senderHostname = senderInfo.substr ( usernameIndex + 1 );

                // get command
                int commandSpaceIndex = commandInfo.find ( " ", senderSpaceIndex + 1 );
                string command = commandInfo.substr ( senderSpaceIndex + 1, commandSpaceIndex - senderSpaceIndex - 1 );

                // get target
                string target = commandInfo.substr ( commandSpaceIndex + 1 );

                // handle privmsg
                if ( command == "PRIVMSG" ) {
                    // check if it is a ctcp request
                    if ( ( message.substr ( 0,1 ) == "\001" ) && ( message.substr ( message.length() - 1,1 ) == "\001" ) ) {
                        // strip off \001's
                        string request = message.substr ( 1, message.length() - 2 );

                        // check if it was an action
                        if ( request.find ( "ACTION" ) == 0 ) {
                            // strip ACTION from request
                            string action = request.substr ( 7 );

                            // print debug and call handler
                            debugMessage ( " *" + senderNickname + " " + action );
                            onAction ( target, action, senderNickname, senderUsername, senderHostname );
                        }

                        // print debug and call handler
                        debugMessage ( "--- revieved ctcp request from " + senderNickname + ": " + request );
                        onCtcpRequest ( target, request, senderNickname, senderUsername, senderHostname );
                    } else {
                        // print debug and call handler
                        debugMessage ( "<" + senderNickname + "|" + target + "> " + message );
                        onMessage ( target, message, senderNickname, senderUsername, senderHostname );
                    }
                }

                // handle notice
                if ( command == "NOTICE" ) {
                    // check if it is a ctcp response
                    if ( ( message.substr ( 0,1 ) == "\001" ) && ( message.substr ( message.length() - 1,1 ) == "\001" ) ) {
                        // strip off \001's
                        string response = message.substr ( 1, message.length() - 2 );

                        // print debug and call handler
                        debugMessage ( "--- revieved ctcp response from " + senderNickname + ": " + response );
                        onCtcpResponse ( target, response, senderNickname, senderUsername, senderHostname );
                    } else {
                        // print debug and call handler
                        debugMessage ( "-" + senderNickname + "|" + target + "- " + message );
                        onNotice ( target, message, senderNickname, senderUsername, senderHostname );
                    }
                }

                // handle nick change
                if ( command == "NICK" ) {
                    // check if it was us
                    if ( senderNickname == nickname ) {
                        // store new nickname
                        this->nickname = message;
                        debugMessage ( "We changed our nick to: " + nickname );
                    }

                    // print debug and call handler
                    debugMessage ( "--- " + senderNickname + " changes nickname to: " + message );
                    onNicknameChange ( senderNickname, message, senderUsername, senderHostname );
                }

                // handle join
                if ( command == "JOIN" ) {
                    // check if it was us
                    if ( senderNickname == nickname ) {
                        // TODO: append to channel list
                    }

                    // print debug and call handler
                    debugMessage ( "--- " + senderNickname + " joins " + message );
                    onJoinChannel ( senderNickname, message, senderUsername, senderHostname );
                }

                // handle part
                if ( command == "PART" ) {
                    // check if it was us
                    if ( senderNickname == nickname ) {
                        // TODO: remove from channel list
                    }

                    // print debug and call handler
                    debugMessage ( "--- " + senderNickname + " parts " + target + ": " + message );
                    onPartChannel ( senderNickname, target, message, senderUsername, senderHostname );
                }

                // handle kick
                if ( command == "KICK" ) {
                    // seperate channel and person (who)
                    int channelIndex = target.find ( " " );
                    string channel = target.substr ( 0, channelIndex );
                    string who = target.substr ( channelIndex + 1 );

                    // check if it is us
                    if ( who == nickname ) {
                        // TODO: remove from channel list
                    }

                    // print debug and call handler
                    debugMessage ( "--- " + senderNickname + " kicked " + who + " from " + channel + ": " + message );
                    onKick ( senderNickname, target, channel, message, senderUsername, senderHostname );
                }

                /*  TODO: handle mode change
                          also call handler for channel mode changed, and user mode changed
                          and of course onOp, deOP, etc .. */


                /* TODO: remove this once everythings figured out */
                debugMessage ( "message: " + message );
                debugMessage ( "command: " + command );
                debugMessage ( "target: " + target );
                debugMessage ( "nickname: " + senderNickname );
                debugMessage ( "username: " + senderUsername );
                debugMessage ( "hostname: " + senderHostname );
            }
        }
    }

    // send debug message
    debugMessage ( "Bot shutting down!" );

    // close socket
    close ( socketFd );

    // return success
    return true;
}

// function to enable or disable debug output
void Irc::setDebugOutput ( bool enable )
{
    // set switch
    debugOutputEnabled = enable;
}

// function to spit out a debug message if debug output is enabled
void Irc::debugMessage ( const string& message ) const
{
    // check if debug output enabled
    if ( debugOutputEnabled ) {
        time_t rawTime;
        struct tm* timeInfo;
        string timeStamp;

        // generate time stamp
        time ( &rawTime );
        timeInfo = localtime ( &rawTime );
        timeStamp = asctime ( timeInfo );
        timeStamp.erase ( timeStamp.length()-1,1 );

        // output debug message
        cout << "[" << timeStamp << "] " << message;
        if ( ( message[message.length()-1] != '\n' ) && ( message[message.length()-1] != '\r' ) ) {
            cout << endl;
        }
    }
}

// function to send a line to the server
bool Irc::sendLine ( const string& line ) const
{
    // send line
    if ( send ( socketFd, string ( line + "\r\n" ).c_str(), line.length() + 2, 0 ) < 0 ) {
        debugMessage ( "Failed to send line to server: " + line );
        return false;
    }

    // output debug info and return success
    debugMessage ( "<<< " + line );
    return true;
}

// function to read a line from the server
bool Irc::readLine ( string& line ) const
{
    int i = 0;
    char ch[2];
    char buffer[READ_BUFFER_LEN];

    // read line character by character
    while ( recv ( socketFd, ch, 1, 0 ) > 0 && ch[0] != EOF ) {
        if ( ch[0] == '\n' ) break;
        buffer[i] = ch[0];
        i++;
    }

    // check if it worked
    if ( i == 0 ) {
        debugMessage ( "No line from server.. we've probably disconnected!" );
        return false;
    }

    // terminate string
    buffer[i] = '\0';

    // copy and clean up line
    line = buffer;
    Irc::findReplace ( line, "\n","" );
    Irc::findReplace ( line, "\r","" );

    // print debug info, and return success
    debugMessage ( ">>> " + line );
    return true;
}

// quick little find replace function
void Irc::findReplace ( string& line, const string& findString, const string& replaceString )
{
    size_t position;

    // loop while we are finding strings to replace
    do {
        // search for findString
        position = line.find ( findString );

        // if string was found, replace it
        if ( position != string::npos ) {
            line.replace ( position, findString.length(), replaceString );
        }
    } while ( position != string::npos );
}

// function to get current believed nick name
const string Irc::getNickname() const
{
    // return believed nickname
    return nickname;
}

// function to stop the bots event loop
void Irc::shutdown()
{
    // set run flag to false and let event loop kill itself
    runFlag = false;
}


/**************************************************************************************************
* Event handlers! These are meant to be overridden (well.. cept ping.. unless you really want to) *
**************************************************************************************************/

// server ping handler
void Irc::onPing ( const string& response )
{
    // send pong response
    debugMessage ( "Recieved server PING... sending PONG!" );
    sendLine ( response );
}

// empty handler for PRIVMSG
void Irc::onMessage ( const string& /*target*/, const string& /*message*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ )
{
}

// empty handler for NOTICE
void Irc::onNotice ( const string& /*target*/, const string& /*message*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ )
{
}

// empty handler for NICK
void Irc::onNicknameChange ( const string& /*oldNickname*/, const string& /*newNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ )
{
}

// empty handler for disconnect event (called right before bot terminates)
void Irc::onDisconnect()
{
}

// empty handler for on connect event
void Irc::onConnect()
{
}

// empty handler for on raw line handler
void Irc::onRawLine ( const string& /*line*/ )
{
}

// empty handler for ctcp requests
void Irc::onCtcpRequest ( const string& /*target*/, const string& /*request*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ )
{
}

// empty handler for ctcp responses
void Irc::onCtcpResponse ( const string& /*target*/, const string& /*response*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ )
{
}

// empty handler for actions
void Irc::onAction ( const string& /*target*/, const string& /*message*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ )
{
}

// empty handler for joins
void Irc::onJoinChannel ( const string& /*senderNickname*/, const string& /*channel*/, const string& /*senderUsername*/, const string& /*senderHostname*/ )
{
}

// empty handler for parts
void Irc::onPartChannel ( const string& /*senderNickname*/, const string& /*channel*/, const string& /*reason*/, const string& /*senderUsername*/, const string& /*senderHostname*/ )
{
}

// empty handler for kicks
void Irc::onKick ( const string& /*senderNickname*/, const string& /*kickedNickname*/, const string& /*channel*/, const string& /*message*/, const string& /*senderUsername*/, const string& /*senderHostname*/ )
{
}



/*********************************************************************
* IRC Functions! These are for doing various things on/to the server *
*********************************************************************/

// function to send a raw line to the server
void Irc::sendRawLine ( const string& line ) const
{
    // send line to server
    sendLine ( line );
}

// function to send a privmsg
void Irc::sendMessage ( const string& target, const string& message ) const
{
    // send to server
    sendLine ( "PRIVMSG " + target + " :" + message );
}

// function to join a channel
void Irc::joinChannel ( const string& channel, const string& key ) const
{
    // send to server
    sendLine ( "JOIN " + channel + " " + key );
}

// function to part a channel
void Irc::partChannel ( const string& channel, const string& reason ) const
{
    // send to server
    sendLine ( "PART " + channel + " :" + reason );
}

// function to quit server
void Irc::quit ( const string& message ) const
{
    // send to server
    sendLine ( "QUIT :" + message );
}

// function to change your nickname
void Irc::changeNickname ( const string& newNickname ) const
{
    // send to server
    sendLine ( "NICK " + newNickname );
}

// function to set a mode on a channel
void Irc::setChannelMode ( const string& channel, const string& modeLine ) const
{
    // send to server
    sendLine ( "MODE " + channel + " " + modeLine );
}

// function to set mode on a user
void Irc::setUserMode ( const string& nickname, const string& modeLine ) const
{
    // send to server
    sendLine ( "MODE " + nickname + " " + modeLine );
}

// function to give someone operator privileges
void Irc::op ( const string& channel, const string& nickname ) const
{
    // set mode +o
    setChannelMode ( channel, "+o " + nickname );
}

// function to remove operator privileges from someone
void Irc::deop ( const string& channel, const string& nickname ) const
{
    // set mode -o
    setChannelMode ( channel, "-o " + nickname );
}

// function to give voice privileges to someone
void Irc::voice ( const string& channel, const string& nickname ) const
{
    // set mode +v
    setChannelMode ( channel, "+v " + nickname );
}

// fucntion to remove voice privileges from someone
void Irc::devoice ( const string& channel, const string& nickname ) const
{
    // set mode -v
    setChannelMode ( channel, "-v " + nickname );
}

// function to ban a hostmask from the channel
void Irc::ban ( const string& channel, const string& hostmask ) const
{
    // set mode +b
    setChannelMode ( channel, "+b " + hostmask );
}

// function to remove a ban from the channel
void Irc::removeBan ( const string& channel, const string& hostmask ) const
{
    // set mode -b
    setChannelMode ( channel, "-b " + hostmask );
}

// function to kick a user from a channel
void Irc::kick ( const string& channel, const string& nickname, const string& reason ) const
{
    // send to server
    sendLine ( "KICK " + channel + " " + nickname + " :" + reason );
}

// function to send a notice
void Irc::sendNotice ( const string& target, const string& message ) const
{
    // send to server
    sendLine ( "NOTICE " + target + " :" + message );
}

// function to send an action
void Irc::sendAction ( const string& target, const string& message ) const
{
    // send to server
    sendLine ( "PRIVMSG " + target + " :\001ACTION " + message + "\001" );
}

// function to send a ctcp request
void Irc::sendCtcpRequest ( const string& target, const string& request ) const
{
    // send line to server
    sendLine ( "PRIVMSG " + target + " :\001" + request + "\001" );
}

// function to send a ctcp response
void Irc::sendCtcpResponse ( const string& target, const string& response ) const
{
    // send line to server
    sendLine ( "NOTICE " + target + " :\001" + response + "\001" );
}





