/**********************************************************************
* Copyright (C) 2006 by Andre                                         *
*                                                                     *
* This library is free software; you can redistribute it and/or       *
* modify it under the terms of the GNU Lesser General Public          *
* License as published by the Free Software Foundation; either        *
* version 2.1 of the License, or (at your option) any later version.  *
*                                                                     *
* This library is distributed in the hope that it will be useful,     *
* but WITHOUT ANY WARRANTY; without even the implied warranty of      *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   *
* Lesser General Public License for more details.                     *
*                                                                     *
* You should have received a copy of the GNU Lesser General Public    *
* License along with this library; if not, write to the Free Software *
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,          *
* MA  02110-1301  USA                                                 *
**********************************************************************/

#ifndef IRC_H
#define IRC_H

#include <string>

using namespace std;

namespace ARGIrc
{
const int READ_BUFFER_LEN = 1024;

class Irc
{
public:
    // constructor/destructor
    Irc();
    virtual ~Irc();

    // methods
    void setDebugOutput ( bool enable = true );
    bool start ( const string& serverHostname, const string& serverPassword, const int serverPort, const string& nickname, const string& userName = "ARGIrc", const string& realName = "ARGIrc" );

protected:
    // handler methods
    virtual void onPing ( const string& response );
    virtual void onMessage ( const string& /*target*/, const string& /*message*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onNotice ( const string& /*target*/, const string& /*message*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onNicknameChange ( const string& /*oldNickname*/, const string& /*newNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onDisconnect();
    virtual void onConnect();
    virtual void onRawLine ( const string& /*line*/ );
    virtual void onCtcpRequest ( const string& /*target*/, const string& /*request*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onCtcpResponse ( const string& /*target*/, const string& /*response*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onAction ( const string& /*target*/, const string& /*message*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onJoinChannel ( const string& /*senderNickname*/, const string& /*channel*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onPartChannel ( const string& /*senderNickname*/, const string& /*channel*/, const string& /*reason*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onKick ( const string& /*senderNickname*/, const string& /*kickedNickname*/, const string& /*channel*/, const string& /*message*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );

    // irc methods
    void sendRawLine ( const string& line ) const;
    void sendMessage ( const string& target, const string& message ) const;
    void joinChannel ( const string& channel, const string& key = "" ) const;
    void partChannel ( const string& channel, const string& reason = "" ) const;
    void quit ( const string& message = "" ) const;
    void changeNickname ( const string& newNickname ) const;
    void setChannelMode ( const string& channel, const string& modeLine ) const;
    void setUserMode ( const string& nickname, const string& modeLine ) const;
    void op ( const string& channel, const string& nickname ) const;
    void deop ( const string& channel, const string& nickname ) const;
    void voice ( const string& channel, const string& nickname ) const;
    void devoice ( const string& channel, const string& nickname ) const;
    void ban ( const string& channel, const string& hostmask ) const;
    void removeBan ( const string& channel, const string& hostmask ) const;
    void kick ( const string& channel, const string& nickname, const string& reason = "" ) const;
    void sendNotice ( const string& target, const string& message ) const;
    void sendAction ( const string& target, const string& message ) const;
    void sendCtcpRequest ( const string& target, const string& request ) const;
    void sendCtcpResponse ( const string& target, const string& response ) const;

    // misc methods
    const string getNickname() const;
    void debugMessage ( const string& message ) const;
    void shutdown();

private:
    // methods
    bool sendLine ( const string& line ) const;
    bool readLine ( string& line ) const;
    static void findReplace ( string& line, const string& findString, const string& replaceString );

    // vars
    bool debugOutputEnabled;
    int socketFd;
    string nickname;
    bool runFlag;
};
}

#endif


