#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "src/CSession.h"

const char* VERSION = "0.002a";


int main ( int argc, char **argv )
{
    CSession Session;
    SServer Server;
    int option;
    int fork_id=0;
    Session.Version=VERSION;


    if(argc>1) {
        while ( ( option = getopt ( argc, argv,"h:a:p:n:u:r:w:c:?d" ) ) != -1 ) {
            switch ( option ) {
            default:
            case '?':
                fprintf ( stderr, "Help:\n"
                          "-h\tServername\tIf used without other arguments,"
                          " it uses the Setting from the configurationfile.\n"
                          "-a\tAdresse\n"
                          "-p\tPort\n"
                          "-n\tNickname\n"
                          "-u\tUsername\n"
                          "-r\tRealname\n"
                          "-w\tPassword\n"
                          "-c\tStandard channel\n"
                          "-d\tDaemonize\n"
                        );
                exit ( EXIT_FAILURE );
            case 'h':
                if(optarg!=nullptr)
                    Server.Name = optarg;
                printf ( "Servername:%s ", Server.Name.c_str());
                break;
            case 'a':
                if(optarg!=nullptr)
                    Server.Hostname = optarg;
                printf ( "Server:%s ", Server.Hostname.c_str() );
                break;
            case 'p':
                if(optarg!=nullptr)
                    Server.Port = atoi(optarg);
                printf ( "Port:%i ", Server.Port );
                break;
            case 'n':
                if(optarg!=nullptr)
                    Server.nickName = optarg;
                printf ( "Nickname:%s ", Server.nickName.c_str());
                break;
            case 'u':
                if(optarg!=nullptr)
                    Server.userName = optarg;
                printf ( "Username:%s ", Server.userName.c_str());
                break;
            case 'r':
                if(optarg!=nullptr)
                    Server.realName = optarg;
                printf ( "Realname:%s ", Server.realName.c_str());
                break;
            case 'w':
                if(optarg!=nullptr)
                    Server.Password=optarg;
                printf ( "Password:%s ", Server.Password.c_str());
                break;
            case 'c':
                if(optarg!=nullptr)
                {
                    Server.stdChannel=optarg;
                }
                printf ( "Channel:%s ", optarg);
                break;
            case 'd':
                printf ( "\n!Daemonized!\n");
                fork_id=fork();
                if(fork_id<0)exit(EXIT_FAILURE);
                if(fork_id>0)exit(0);
                break;
            }
        }
        printf("\n");
    }

    if(Server.Filled()) {
        if(Server.Validation()) {
            Session.Configuration->addServer(Server);
            Session.Connection->Start(Server);
            printf("Remembering this server under same name!\n");
        } else if (!Server.Name.empty()) {
            Server=Session.Configuration->getServer(Server.Name);
            Session.Connection->Start(Server);
        } else
        {
            printf("not enough parameters: serveradress is enough!\nparameters will be saved in settings file!\n");
        }
    } else {
        printf("using default server settings\n");
        Server = Session.Configuration->getServer();
        if(Server.Validation())
            Session.Connection->Start(Server);
        else
            printf("default server got not enough parameters!\n");
    }
    return 0;
}
