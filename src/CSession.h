/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef BOT_H
#define BOT_H

#include "ETypes.h"
#include <ctime>

#include "CConfig.h"
#include "CParser.h"
#include "CPluginManager.h"
#include "CConnection.h"

class CPluginManager;
class CConfig;
class CConnection;
class CParser;
class CSession
{
    time_t timer;
public:
    SUser Admin;
    map<string,SUser> Users;
    string TimeString(time_t time, const char* format);
    time_t getCurrentTime();
    string Version;
    CPluginManager *PluginManager;
    CConfig *Configuration;
    CConnection *Connection;
    CParser *Parser;

    CSession();
    ~CSession();

    /**
    *Check if Flag is set for EParsing
    *@param Var EParsing-collection.
    *@param Value EParsing to be checked for.
    *@return returns true if Value is found in Var.
    */
    inline bool FlagSet(EParsing Var, EParsing Value) {
        return ((Var & Value) == (Value));
    }

    /**
     *Check if Flag is set for ERights
     *@param Var ERights-collection.
     *@param Value ERights to be checked for.
     *@return returns true if Value is found in Var.
     */
    inline bool FlagSet(ERights Var, ERights Value) {
        return ((Var & Value) == (Value));
    }
};

#endif // BOT_H
