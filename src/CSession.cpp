/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CSession.h"

CSession::CSession() : Admin("")
{
    Configuration=new CConfig(this);
    PluginManager=new CPluginManager(this,Configuration->GetPluginDir());
    Parser=new CParser(this);
    Connection=new CConnection(this);

    int plugincount=PluginManager->LookupPlugins();
    printf("%i plugin%c found!\n",plugincount,plugincount!=1?'s':0);
    PluginManager->Start();

    pair<string,string> AdminConfig = Configuration->getAdmin();
    Admin.Nick=AdminConfig.first;
    Admin.Password=AdminConfig.second;

    printf("\nSession started!\n");
}

time_t CSession::getCurrentTime()
{
    time(&timer);
    return timer;
}

string CSession::TimeString(time_t time, const char* format)
{
    string Result;
    tm * timeptr;
    char* buffer=0;


    timeptr=gmtime(&time);

    strftime(buffer,256,format,timeptr);

    Result=buffer;
    return Result;
}


CSession::~CSession()
{
    delete PluginManager;
    delete Configuration;
    delete Parser;
    delete Connection;
}
