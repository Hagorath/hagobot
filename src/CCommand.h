/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CCOMMAND_H
#define CCOMMAND_H
#include "default.h"
#include "CBase.h"
#include "SMessage.h"
#include <vector>

class CCommand : public CBase
{
public:
    CCommand(CSession* Session) : CBase(Session) {}
    virtual EParsing Run(const SMessage Message,const vector<string> & Split)
    {
        printf("%s Command from %s : %s in %s",Split[0].c_str(),Message.senderNickname.c_str(),Message.Value.c_str(),Message.Target.c_str());
        return EParsing::processed;
    }
};

#endif // CCOMMAND_H
