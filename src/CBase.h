/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CBASE_H
#define CBASE_H
#include <string>
#include <cstring>

class CSession;
class CBase
{
public:
    std::string ConvertToString(const unsigned char* chararray);
    CSession *Session;
    CBase(CSession *Session);
};

#endif // CBASE_H
