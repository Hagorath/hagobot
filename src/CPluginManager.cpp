/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CPluginManager.h"
#include "CSession.h"

CPluginManager::CPluginManager(CSession* Session, string Pluginpath) : CBase(Session)
{
    this->Pluginpath=Pluginpath;
    dir=nullptr;
}

int CPluginManager::LookupPlugins()
{
    string tmpname;
    dir=opendir(Pluginpath.c_str());
    if(dir==nullptr) {
        printf("Plugin path not found, creating %s...\n",Pluginpath.c_str());

        if(makedir(Pluginpath.c_str())!=0) {
            return -1;
        } else
        {
            return PluginVector.size();
        }
    }

    while( (dirinfo=readdir(dir)) ) {
        tmpname=dirinfo->d_name;
        if( tmpname.size()>3 && (tmpname.substr(tmpname.size()-3).compare(".so") == 0) ) {
            //printf("%s ", tmpname.c_str());
            PluginVector.push_back(tmpname);
        }
    }
    closedir(dir);
    return PluginVector.size();
}

int CPluginManager::makedir(string Path)
{
    int result= mkdir((Path.c_str()),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if(result==0 || result==EEXIST) {
        return 0;
    } else {
        printf("Could not create Path:");
        switch(result) {
        default:
            printf(" Some random special-case...\n");
            break;
        case EACCES:
            printf(" No write permission!\n");
            break;
        case EROFS:
            printf(" Read-only file system!\n");
            break;
        case ENOSPC:
            printf(" no enough space!\n");
        case ENOENT:
        case ENOTDIR:
            printf(" invalid path string!\n");
        }
    }
    return 1;
}

vector< string >* CPluginManager::GetPlugins()
{
    return &PluginVector;
}


void CPluginManager::Start()
{
    LookupPlugins();
    vector<string> active_vector = Session->Configuration->GetActivatedPlugins();
    for(unsigned int i=0; i<active_vector.size(); i++) {
        if(find(PluginVector.begin(), PluginVector.end(), active_vector[i]) != PluginVector.end()) {
            LoadPlugin(active_vector[i]);
        }
        else
            continue;
    }
}

int CPluginManager::LoadPlugin(string Plugin)
{
    string PluginDir=Pluginpath;
    PluginDir+="/";
    PluginDir+=Plugin;
    printf("Loading %s...\n",Plugin.c_str());
    void *handle= dlopen(PluginDir.c_str(),RTLD_LAZY);
    if(!handle) {
        printf("Couldn't open %s...\n",Plugin.c_str());
        const char* dlsym_error=dlerror();
        if(dlsym_error) {
            printf("%s\n",dlsym_error);
        }
        return 1;
    } else
    {
        dlerror();
        SPlugin::construct_plugin_t* constructor = (SPlugin::construct_plugin_t *)(dlsym(handle,"construct"));
        SPlugin::destruct_plugin_t* destructor = (SPlugin::destruct_plugin_t *)(dlsym(handle,"destruct"));

        const char* dlsym_error=dlerror();
        if(dlsym_error) {
            printf("Cannot load %s-Symbol!\n",dlsym_error);
            return 1;
        } else {
            //SPlugin* plugin= PluginMap.insert(pair<string,SPlugin*>(Plugin,SPlugin_t(constructor(Session),destructor))).first->second.first;
            ConstDestMapPair Insertion=pair<string,SPlugin_t>(Plugin,SPlugin_t(constructor(Session),destructor));
            if(CheckPlugin(Insertion.first)==0)
                PluginMap.insert(Insertion);
            else
                return 1;
            Insertion.second.first->Init();
            Insertion.second.first->Start();
        }
        return 0;
    }
    return 1;
}

int CPluginManager::UnloadPlugin(string Plugin)
{
    ConstDestMap::iterator iter;
    for(iter=PluginMap.begin(); iter!=PluginMap.end(); iter++) {
        if(iter->first.compare(Plugin)==0) {
            Session->Configuration->DeactivatePlugin(iter->first);
            (*(iter->second.second))(iter->second.first);
            PluginMap.erase(iter);
            return 0;
        }
    }
    return 1;
}

int CPluginManager::CheckPlugin(string Plugin)
{
    for(ConstDestMap::iterator iter=PluginMap.begin(); iter!=PluginMap.end(); iter++) {
        if(iter->first.compare(Plugin)==0) {
            printf("Library already loaded, ignoring request!\n");
            return 1;
        }
    }
    return 0;
}


CPluginManager::~CPluginManager()
{

}
