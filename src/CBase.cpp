/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CBase.h"

CBase::CBase(CSession *Session)
{
    this->Session=Session;

};

/**
 * Converts Char to String.
 * @param chararray c-styled dynamic-string (unsigned char*).
 * @return c++-styled string.
 */
std::string CBase::ConvertToString(const unsigned char* chararray) {
    std::string Result;
    int size=std::char_traits< unsigned char>::length(chararray);
    Result.reserve(size);
    for(int i=0; i<size; i++) {
        Result+=chararray[i];
    }
    return Result;
}
