/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CCONNECTION_H
#define CCONNECTION_H
#include "default.h"
#include "ETypes.h"
#include <Irc.h>

#include "SUser.h"
#include "SMessage.h"
#include "SChannel.h"
#include "SServer.h"

#include "CBase.h"
#include "CSession.h"
#include "CParser.h"
#include "IEventHandler.h"

using namespace ARGIrc;

class CConnection : public Irc, public CBase, public IEventHandler
{
    char* getHostname();
    void onAction ( const string& target, const string& message, const string& senderNickname, const string& senderUsername, const string& senderHostname );
    void onConnect();
    void onDisconnect();
    void onCtcpRequest 		( const string& , const string& , const string& , const string& , const string& );
    void onCtcpResponse 	( const string& , const string& , const string& , const string& , const string& );
    void onJoinChannel 		( const string& senderNickname, const string& channel, const string& senderUsername, const string& senderHostname);
    void onKick 		( const string& senderNickname, const string& kickedNickname, const string& Channel, const string& reason, const string& senderUsername, const string& senderHostname);
    void onMessage 		( const string& target, const string& message, const string& senderNickname, const string& senderUsername, const string& senderHostname );
    void onNicknameChange 	( const string& oldNickname, const string& newNickname, const string& senderUsername, const string& senderHostname);
    void onNotice 		( const string& target, const string& message, const string& senderNickname, const string& senderUsername, const string& senderHostname);
    void onPartChannel 		( const string& senderNickname, const string& channel, const string& reason, const string& senderUsername, const string& senderHostname);
    void onRawLine 		( const string& );

    SUser Self;
    string Self_realName;
public:
    bool DIRTY_NICK_SAVE;
    CConnection(CSession *Session);
    void Start(SServer StartServer);
    SUser getSelf();
    SServer Server;
    string getNickname();
    void ChangeNickname(const string& newNickname);
    void JoinChannel(const string& channel, const string& key);
    void sendMessage(const string& target, const string& message);
    void Disconnect(const string& reason="");
    void kickUser(const string& Channel, const string& NickName, const string& reason="");


public:

    virtual void Registry(EEvent Event, SPlugin* Plugin);
    virtual void Depart(EEvent Event, SPlugin* Plugin );
    virtual void onEvent(const SMessage& Message, const EEvent Event);
    ~CConnection();
};

#endif // CCONNECTION_H
