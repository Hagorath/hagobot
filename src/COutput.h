/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef COUTPUT_H
#define COUTPUT_H
#include "default.h"

/**
 * Output-struct for the CReleaser to give different Release Sources
 *
 */
struct COutput {
    virtual void onPing ( const string& response );
    virtual void onMessage ( const string& /*target*/, const string& /*message*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onNotice ( const string& /*target*/, const string& /*message*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onNicknameChange ( const string& /*oldNickname*/, const string& /*newNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onDisconnect();
    virtual void onConnect();
    virtual void onRawLine ( const string& /*line*/ );
    virtual void onCtcpRequest ( const string& /*target*/, const string& /*request*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onCtcpResponse ( const string& /*target*/, const string& /*response*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onAction ( const string& /*target*/, const string& /*message*/, const string& /*senderNickname*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onJoinChannel ( const string& /*senderNickname*/, const string& /*channel*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onPartChannel ( const string& /*senderNickname*/, const string& /*channel*/, const string& /*reason*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
    virtual void onKick ( const string& /*senderNickname*/, const string& /*kickedNickname*/, const string& /*channel*/, const string& /*message*/, const string& /*senderUsername*/, const string& /*senderHostname*/ );
};

#endif // COUTPUT_H
