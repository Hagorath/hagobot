/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CConnection.h"

CConnection::CConnection(CSession *Session) : CBase(Session), Self("")
{
    DIRTY_NICK_SAVE=false;
}


void CConnection::Start(SServer StartServer)
{
    Self.Nick=StartServer.nickName;
    Self.User=StartServer.userName;
    Self_realName=StartServer.realName;

    //Self.Host=getHostname();
    Server=StartServer;

    printf("START:\n%s\n",StartServer.toString().c_str());
    if(!start ( StartServer.Hostname,StartServer.Password,StartServer.Port,StartServer.nickName, "HagorathBot", "HagorathBot" ))
        printf("Can't connect!\n");
}

string CConnection::getNickname()
{
    return Irc::getNickname();
}

/**
 *
 *BUGGY!
 */
char* CConnection::getHostname()
{
    char* hostname=nullptr;
    //char* hostname[1024];
    //hostname[1023] = '\0';
    gethostname(hostname, 1023);
    return hostname;
}



CConnection::~CConnection()
{

}

void CConnection::onCtcpRequest(const string& , const string& , const string& , const string& , const string& )
{
    // ARGIrc::Irc::onCtcpRequest(, , , , );
}


void CConnection::onCtcpResponse(const string& , const string& , const string& , const string& , const string& )
{
    // ARGIrc::Irc::onCtcpResponse(, , , , );
}


void CConnection::onRawLine(const string& )
{
}

void CConnection::sendMessage(const string& target, const string& message)
{
    Irc::sendMessage(target,message);
}

void CConnection::JoinChannel(const string& channel, const string& key)
{
    Irc::joinChannel(channel,key);
}

void CConnection::ChangeNickname(const string& newNickname)
{
    Irc::changeNickname(newNickname);
}


void CConnection::onAction ( const string& target, const string& message, const string& senderNickname, const string& senderUsername, const string& senderHostname )
{
    onEvent(SMessage(target,senderNickname,message,senderUsername,senderHostname,"",EMessageFlag::none),EEvent::action);
}

void CConnection::onConnect()
{
    onEvent(SMessage(),EEvent::connected);
    printf("Connected to %s:  %s.\n",Server.Name.c_str(),Server.Hostname.c_str());
    if(!Server.stdChannel.empty())
        joinChannel(Server.stdChannel,"");
}

void CConnection::Disconnect(const string& reason)
{
    Irc::quit(reason);
}

void CConnection::onDisconnect()
{
    onEvent(SMessage(),EEvent::disconnected);
    printf("Closing Connection.");
}

void CConnection::onJoinChannel ( const string& senderNickname, const string& channel, const string& senderUsername, const string& senderHostname )
{
    onEvent(SMessage(channel,senderNickname,"",senderUsername,senderHostname,"",EMessageFlag::join),EEvent::joinchannel);
}

void CConnection::onKick ( const string& senderNickname, const string& kickedNickname, const string& Channel, const string& reason, const string& senderUsername, const string& senderHostname )
{
    onEvent(SMessage(Channel,senderNickname,kickedNickname,senderUsername,senderHostname,reason,EMessageFlag::kick),EEvent::kick);
}

void CConnection::onMessage ( const string& target, const string& message, const string& senderNickname, const string& senderUsername, const string& senderHostname )
{
    SMessage Message(target,senderNickname,message,senderUsername,senderHostname,"",EMessageFlag::message);
    onEvent(Message,EEvent::message);
    EParsing result = Session->Parser->Parse(Message);
    if(Session->FlagSet(result,EParsing::identified)) {
    }
    if(Message.Value.compare("verpiss dich du Spasst!")==0) {
        sendMessage ( target,senderNickname+": "+Message.Value );
        quit("Verpiss dich selber!");
        shutdown();
    }
}
void CConnection::kickUser(const string& Channel, const string& NickName, const string& reason)
{
    Irc::kick(Channel, NickName, reason);
}

SUser CConnection::getSelf()
{
    return Self;
}


void CConnection::onNicknameChange ( const string& oldNickname, const string& newNickname, const string& senderUsername, const string& senderHostname )
{
    if(newNickname.compare(Self.Nick)) {
        Self.Nick=newNickname;

        if(Session->Parser->getNameReaction()) {
            Session->Parser->setNameReaction();
        }

        if(DIRTY_NICK_SAVE) {
            Session->Parser->GetCommand("nick")->Run(SMessage("Callback"),vector<string>());
            DIRTY_NICK_SAVE=false;
        }

        onEvent(SMessage("",newNickname,oldNickname,senderUsername,senderHostname,"",EMessageFlag::nickchange),EEvent::nickchange);
    }
}

void CConnection::onNotice ( const string& target, const string& message, const string& senderNickname, const string& senderUsername, const string& senderHostname )
{
    onEvent(SMessage(target,senderNickname,message,senderUsername,senderHostname,"",EMessageFlag::notice),EEvent::notice);
}

void CConnection::onPartChannel ( const string& senderNickname, const string& channel, const string& reason, const string& senderUsername, const string& senderHostname )
{
    onEvent(SMessage(channel,senderNickname,reason,senderUsername,senderHostname,"",EMessageFlag::leave),EEvent::partchannel);
}


void CConnection::onEvent(const SMessage& Message, const EEvent Event)
{
    for(IEventHandler::PluginMap::iterator iter=EventMap.begin(); iter!=EventMap.end(); iter++) {
        if(iter->first==Event) {
            if(iter->second!=nullptr)
                iter->second->onEventTrigger(Message,Event);
        }
    }
}


void CConnection::Depart(EEvent Event , SPlugin* Plugin)
{
    for(IEventHandler::PluginMap::iterator iter=EventMap.begin(); iter!=EventMap.end(); iter++) {
        if(iter->first==Event && iter->second==Plugin) {
            EventMap.erase(iter);
        }
    }
}

void CConnection::Registry(EEvent Event , SPlugin* Plugin )
{
    EventMap.insert(std::pair<EEvent,SPlugin*>(Event,Plugin));
}
