/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CConfig.h"

CConfig::CConfig(CSession *Session) : CBase(Session)
{
    printf("Config Class init\n");
    if(access(CONFIGURATION_FILE, F_OK)!=0) {
        Root = &(cfg.getRoot());
        Init();
        printf("No Settings!\n");
    } else {
        try {
            cfg.readFile(CONFIGURATION_FILE);
            Root = &(cfg.getRoot());
        } catch(libconfig::FileIOException& fioex) {
            printf("I/O error while reading file.\n" );
            exit (EXIT_FAILURE);
        } catch(libconfig::ParseException& pex) {
            printf("Parse error in Settings: %i - %s\n", pex.getLine(), pex.getError());
            exit(EXIT_FAILURE);
        }
    };
    if(!Root->exists(CONFIGURATION_MAIN)) {
        Init();
    } else {
        Main= &((*Root)[CONFIGURATION_MAIN]);
    }

    if(!Root->exists(CONFIGURATION_APLUGINS)) {
        ActivatedPlugins=&(Root->add(CONFIGURATION_APLUGINS, Setting::TypeList));
    } else {
        ActivatedPlugins= &((*Root)[CONFIGURATION_APLUGINS]);
    }
}

void CConfig::Init()
{
    Main = &(Root->add(CONFIGURATION_MAIN, Setting::TypeGroup));

    Main->add("AdminNick", Setting::TypeString)="Admin";
    Main->add("AdminPassword", Setting::TypeString)="Admin";
    Main->add("StandardServer", Setting::TypeString)="";

    Main->add("PluginPath", Setting::TypeString)="./Plugins";
    Main->add("Command", Setting::TypeString)="!";
    Main->add("NameReaction", Setting::TypeBoolean)=false;
    Save();
}
int CConfig::Save()
{
    printf("Saving configuration...    ");
    try {
        cfg.writeFile(CONFIGURATION_FILE);
        printf("Done!\n");
        return 0;
    } catch (FileIOException &ex) {
        printf("Failed to save configuration!\n");
    }
    return 1;
}


CConfig::~CConfig()
{

}

string CConfig::GetPluginDir()
{
    string Result;
    Main->lookupValue("PluginPath",Result);
    return Result;
}

bool CConfig::GetNameReaction()
{
    bool Result=false;
    Main->lookupValue("NameReaction",Result);
    return Result;
}

string CConfig::GetStdCommand()
{
    string Result="!";
    Main->lookupValue("Command",Result);
    return Result;
}



int CConfig::ActivatePlugin(string Plugin)
{
    string callback;
    ActivatedPlugins->lookupValue(Plugin,callback);
    if(callback.empty()) {
        ActivatedPlugins->add(Setting::TypeString)=Plugin;
        Save();
        return 0;
    } else
        return 1;
}

vector< string > CConfig::GetActivatedPlugins()
{
    vector<string> Result;
    for(int i=0; (ActivatedPlugins->getLength())>i; i++) {
        string test=(*ActivatedPlugins)[i];
        Result.push_back(test);
    }

    return Result;
}


int CConfig::DeactivatePlugin(string Plugin)
{
    string callback;
    ActivatedPlugins->lookupValue(Plugin,callback);
    if(callback.empty()) {
        ActivatedPlugins->remove(Plugin);
        return 0;
    }
    return 1;
}

int CConfig::ChangeNick(string Server, string Name)
{
    string ServerName=MakeServerName(Server);
    if(Root->exists(ServerName)) {
        if(!(*Root)[ServerName].exists("Nickname")) {
            ((*Root)[ServerName]).add(Name,Setting::TypeString);
        }
        else {
            ((Setting&)((*Root)[ServerName])["Nickname"])=Name;
        }
        Save();
        return 0;
    } else
        return 1;
}


string CConfig::NameLoop(string Name)
{
    int i=0;
    string Fini;
    while(true) {
        i++;
        if(!Root->exists(Fini=Name+to_string(i))) {
            return Fini;
        }
    }
}

string CConfig::MakeServerName(string Name)
{
    string Result=CONFIGURATION_SERVER+"_"+Name;
    return Result;
}

string CConfig::RemoveServerPre(string Name)
{
    return Name.substr(CONFIGURATION_SERVER.length()+1);
}



int CConfig::addServer(string serverName , const string serverHostname , const int serverPort , const string serverPassword, const string nickName, const string userName, const string realName, const string stdChannel, bool def)
{
    Setting *Server;
    string FinishedName;
    if(serverName.empty())
    {
        printf("Kein Name angegeben...");
        serverName="NoName";
        FinishedName=NameLoop(MakeServerName(serverName));

    } else if(Root->exists(MakeServerName(serverName))) {
        printf("Existiert, speichere alternativ!\n");
        FinishedName=NameLoop(MakeServerName(serverName));
    }   else
    {
        printf("Server hinzugefügt!");
        FinishedName=MakeServerName(serverName);
    }

    Server = &(Root->add(FinishedName, Setting::TypeGroup));

    if(def)
        ((Setting&)(*Main)["StandardServer"])=FinishedName;


    Server->add("Hostname", Setting::TypeString)= serverHostname;
    Server->add("Port", Setting::TypeInt)= serverPort;
    Server->add("Password", Setting::TypeString)= serverPassword;
    Server->add("Nickname", Setting::TypeString)= nickName;
    Server->add("Username", Setting::TypeString)= userName;
    Server->add("Realname", Setting::TypeString)= realName;
    Server->add("stdChannel", Setting::TypeString)= stdChannel;

    if(Save())
        return 0;
    else
        return 1;
}

pair< string, string > CConfig::getAdmin()
{   pair<string, string> Result;
    (*Main).lookupValue("AdminNick",Result.first);
    (*Main).lookupValue("AdminPassword",Result.second);
    return Result;
}


string CConfig::getStdServer()
{
    string Result;
    (*Root)[CONFIGURATION_MAIN].lookupValue("StandardServer",Result);
    return Result;
}


SServer CConfig::getServer(string serverName)
{
    SServer Result;
    Setting* Server;
    try {
        if(serverName.empty()) {
            printf("std serv\n");
            Server=&((*Root)[getStdServer()]);
        } else {
            Server=&((*Root)[MakeServerName(serverName)]);
            Result.Name=serverName;
        }
        Result.Name=RemoveServerPre(Server->getName());
        Server->lookupValue("Hostname",Result.Hostname);
        Server->lookupValue("Port",Result.Port);
        Server->lookupValue("Password",Result.Password);
        Server->lookupValue("Nickname",Result.nickName);
        Server->lookupValue("Username",Result.userName);
        Server->lookupValue("Realname",Result.realName);
        Server->lookupValue("stdChannel",Result.stdChannel);
    } catch(libconfig::SettingNotFoundException) {
        printf("Wrong Settings/Server!\n");
    }
    //printf("CONFIG: \n%s",Result.toString().c_str());
    return Result;
}


int CConfig::addServer(const SServer& Server, bool def)
{
    return addServer(Server.Name,Server.Hostname,Server.Port,Server.Password,Server.nickName,Server.userName,Server.realName, Server.stdChannel, def);
}


/**
 *does not work right now!
 */
int CConfig::ChangeServer(const string serverName, const string serverHostname, const int serverPort, const string serverPassword, const string nickName, const string userName, const string realName, const string stdChannel, bool def)
{
    return 1;
}

int CConfig::setDefault(const SServer& Server)
{
    ((Setting&)(*Main)["StandardServer"])=MakeServerName(Server.Name);
    return Save();
}
