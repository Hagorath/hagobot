/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CUSER_H
#define CUSER_H
#include "default.h"
#include "CContext.h"
#include "ETypes.h"


/*!
 *Struct of User.
 *used for Rights-Administration and Lookup/comparism
 *
 */
struct SUser {
    string Nick;//!< Nickname of User
    string User;//!< Username
    string Host;//!< Hostname
    string Password; //!Password for Bot
    ERights Rights; //!Rights given for Bot

    /*!Simple Constructor....
     *@param Nick needed
     *@param User empty by default
     *@param Host empty by default
     *@param Password empty by default
     *@param Rights none by default
     */
    SUser(string Nick, string User="", string Host="", string Password="", ERights Rights=ERights::none);
private:
    CContext *Context; //!Context <-not implemented yet->
};

#endif // CUSER_H
