#ifndef ETYPES_H
#define ETYPES_H

enum class EParsing : int {
    none		=0x0  /*Not a single shit was given this day*/,
    identified		=0x01 /*Message starts with Command-Flag*/,
    processed		=0x02 /*Message is Command AND correctly executed */,
    flawed		=0x04 /*Message is Command AND not correctly written*/,
    unrecognized	=0x08 /*Message is with Command-Flag, but does not exist*/,
    nocommand		=0x10 /*Message is no Command*/,
    denied		=0x20 /*denied command!*/,
    infeasible		=0x40 /*couldnt do it :(*/,
    onhold		=0x80 /*Answer is taking a while...*/
};

enum class EMessageFlag : int {
    none,
    message,
    kick,
    join,
    leave,
    nickchange,
    action,
    notice
};

enum class EEvent : char {
    none,
    disconnected,
    connected,
    message,
    nickchange,
    rawline,
    ctcprequest,
    ctcpresonse,
    action,
    joinchannel,
    partchannel,
    kick,
    leave,
    notice
};

inline EParsing operator |= (EParsing &a, EParsing b) {
    unsigned ai = static_cast<unsigned>(a);
    unsigned bi = static_cast<unsigned>(b);
    ai |= bi;
    return a = static_cast<EParsing>(ai);
}
inline EParsing operator & (EParsing &a, EParsing b) {
    unsigned ai = static_cast<unsigned>(a);
    unsigned bi = static_cast<unsigned>(b);
    ai &= bi;
    return a = static_cast<EParsing>(ai);
}


enum class ERights : int {
    none=0x0,
    user		=0x01 /*Allowed to use simple Commands*/,
    moderator		=0x02 /*Allowed to use Commands which changes Bot-Behaviour*/,
    administrator	=0x04 /*Allowed to use Commands which controls Bot-System*/,
    autoop		=0x08 /*Bot gives AutoOp!*/
};

inline ERights operator |= (ERights &a, ERights b) {
    unsigned ai = static_cast<unsigned>(a);
    unsigned bi = static_cast<unsigned>(b);
    ai |= bi;
    return a = static_cast<ERights>(ai);
}

inline ERights operator & (ERights &a, ERights b) {
    unsigned ai = static_cast<unsigned>(a);
    unsigned bi = static_cast<unsigned>(b);
    ai &= bi;
    return a = static_cast<ERights>(ai);
}

#endif /* ETYPES_H */
