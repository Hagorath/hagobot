/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "COutput.h"

void COutput::onAction ( const string& , const string& , const string& , const string& , const string& )
{

}

void COutput::onConnect()
{

}

void COutput::onCtcpRequest ( const string& , const string& , const string& , const string& , const string& )
{

}
void COutput::onJoinChannel ( const string& , const string& , const string& , const string& )
{

}

void COutput::onCtcpResponse ( const string& , const string& , const string& , const string& , const string& )
{

}
void COutput::onDisconnect()
{

}
void COutput::onKick ( const string& , const string& , const string& , const string& , const string& , const string& )
{

}
void COutput::onMessage ( const string& , const string& , const string& , const string& , const string& )
{

}
void COutput::onNicknameChange ( const string& , const string& , const string& , const string& )
{

}
void COutput::onNotice ( const string& , const string& , const string& , const string& , const string& )
{

}
void COutput::onPartChannel ( const string& , const string& , const string& , const string& , const string& )
{

}
void COutput::onPing ( const string& response )
{

}
void COutput::onRawLine ( const string& )
{

}
