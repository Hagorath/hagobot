/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CPARSER_H
#define CPARSER_H
#include <map>
#include <algorithm>
#include<sstream>

#include "default.h"

#include "SMessage.h"
#include "ETypes.h"
#include "CCommand.h"

#include "Commands/CKick.h"
#include "Commands/CVersion.h"
#include "Commands/CJoin.h"
#include "Commands/CQuit.h"
#include "Commands/CNick.h"
#include "Commands/CCommandValue.h"
#include "Commands/CCommandToggle.h"
#include "Commands/CMSG.h"
#include "Commands/CActivatePlugin.h"
#include "Commands/CDeactivatePlugin.h"

class CParser : CBase
{
    string StdCommand;
    string Command;
    map<string,CCommand *> Commandmap;
    vector<string>SplitString(string value);
    bool namereaction;
public:
    bool ToggleReactOnName();
    void setNameReaction(bool val=true);
    bool getNameReaction();
    bool saveCommand(const char* CommandName, CCommand* Command);
    void deleteCommand(const string& CommandName);
    void setCommandLauncher ( string String="" );
    string getStdCommand();
    string getCurCommand();
    CCommand* GetCommand(const string& value);
    EParsing Parse ( SMessage Message );
    CParser(CSession *Session);
    virtual ~CParser();
};

#endif // CPARSER_H
