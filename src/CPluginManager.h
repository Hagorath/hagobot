/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CPLUGINS_H
#define CPLUGINS_H
#include <dlfcn.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <map>
#include <vector>
#include <algorithm>
#include "default.h"
#include "SMessage.h"
#include "SPlugin.h"
#include "CSession.h"


class CPluginManager : CBase
{
    int makedir(string Path);
    typedef map<string, SPlugin_t> ConstDestMap;
    typedef pair<string, SPlugin_t> ConstDestMapPair;
    ConstDestMap PluginMap;
    vector<string> PluginVector;
    string Pluginpath;
    DIR *dir;
    dirent *dirinfo;
public:
    CPluginManager(CSession *Session, string Pluginpath = "./");
    int CheckPlugin(string Plugin);
    int LookupPlugins();
    vector< string >* GetPlugins();
    int LoadPlugin(string Plugin);
    int UnloadPlugin(string Plugin);

    virtual void Start();
    virtual ~CPluginManager();

};

#endif // CPLUGINS_H
