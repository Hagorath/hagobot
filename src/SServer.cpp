/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "SServer.h"

SServer::SServer()
{
    SServer("","",STD_PORT,"hagoBot","","hagoBot","hagoBot");
}

SServer::SServer(string serverName, string serverHostname, int serverPort, string nickName, string serverPassword, string userName, string realName, string stdChannel)
{
    this->Name = serverName;
    this->Hostname=serverHostname;
    this->Port = serverPort;
    this->Password = serverPassword;

    this->nickName = nickName;
    this->userName = userName;
    this->realName = realName;
    this->stdChannel = stdChannel;
}

SServer::~SServer()
{

}

string SServer::toString()
{
    string Result = "Name:    "+ Name +"\n";
    Result+= "Adresse: "+ Hostname +"\n";
    Result+= "Port:    ";
    Result+= to_string(Port);
    Result+="\n";
    Result+= "Password:"+ Password +"\n";
    Result+= "nickName:"+ nickName +"\n";
    Result+= "userName:"+ userName +"\n";
    Result+= "realName:"+ realName +"\n";
    Result+= "Channel :"+ stdChannel +"\n";
    return Result;
}

bool SServer::Validation()
{
    if(!Hostname.empty()) {
        return true;
    }
    return false;
}

bool SServer::Filled()
{
    if(Name.empty() && Hostname.empty() && Password.empty()
            && nickName.empty() && userName.empty() && realName.empty()
            && stdChannel.empty())
        return false;
    else
        return true;
}
