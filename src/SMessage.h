/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CMESSAGE_H
#define CMESSAGE_H
#include "default.h"
#include "ETypes.h"

/**
 *
 *
 *@param Value stands for MessageContent, NoticeContent, NewNickname, Request or Response!
 */
struct SMessage {
    EMessageFlag Flag;
    string Target;
    string senderNickname;
    string senderUsername;
    string senderHostname;
    string Value;
    string Reason;
    SMessage(string Target = "", string senderNickname = "", string Value = "", string senderUsername="", string senderHostname="", string reason="",EMessageFlag Flag=EMessageFlag::none);

    operator const char *() {
        return Value.c_str();
    }
};

#endif // CMESSAGE_H
