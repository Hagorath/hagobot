/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CKick.h"

CKick::CKick(CSession* Session): CCommand(Session)
{

}

EParsing CKick::Run(const SMessage Message, const vector< string >& Split)
{
//     printf("Splitsize: %d\n", Split.size());
    if(Split.size()==2) {
        Session->Connection->kickUser(Message.Target,Split[1], "");
    } else if(Split.size()>2) {
//         if(Message.Owner.compare(Message.Target)==0)
//             Session->Connection->kickUser(Split[1],Split[2], Message.Value.substr(Split[0].size()+Split[1].size()+Split[2].size()));
//         else
        Session->Connection->kickUser(Message.Target,Split[1], Message.Value.substr(Split[0].size()+Split[1].size()+2));
    }
    return EParsing::processed;
}
