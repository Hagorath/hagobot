/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CQuit.h"

CQuit::CQuit(CSession* Session): CCommand(Session)
{

}

EParsing CQuit::Run(const SMessage Message, const vector< string >& Split)
{
    Session->Connection->Disconnect(Split.size()>1?Message.Value:"");
    return EParsing::processed;
    return EParsing::denied;
}
