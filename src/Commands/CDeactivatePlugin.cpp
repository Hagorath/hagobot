/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CDeactivatePlugin.h"

CDeactivatePlugin::CDeactivatePlugin(CSession* Session): CCommand(Session)
{

}

EParsing CDeactivatePlugin::Run(const SMessage Message, const std::vector< std::string, std::allocator< std::string > >& Split)
{
    string Feedback;
    if( Split.size()>1 ) {
        if(Session->PluginManager->UnloadPlugin(Split[1])==0) {
            Feedback="unloaded "+ Split[1] + " successfully!";
            Session->Connection->sendMessage(Message.Target,Feedback);
            return EParsing::processed;
        } else {
            Feedback="could not unload " + Split[1] + " completly!";
            Session->Connection->sendMessage(Message.Target,Feedback);
            return EParsing::infeasible;
        }
    }

    return EParsing::flawed;
}

