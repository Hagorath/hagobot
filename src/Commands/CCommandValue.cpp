/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CCommandValue.h"

CCommandValue::CCommandValue(CSession* Session): CCommand(Session)
{

}

EParsing CCommandValue::Run(const SMessage Message, const vector< string >& Split)
{
    if(Split.size()>1) {
        Session->Parser->setCommandLauncher(Split[1]);
        string val="Reacting now on \"";
        val+=Split[1];
        val+="\"!";
        if(Session->Parser->getNameReaction()) {
            val+=" Changing myself back to command mode!";
            Session->Parser->setNameReaction(false);
        }

        Session->Connection->sendMessage(Message.Target,val);
    }
    return EParsing::denied;
}
