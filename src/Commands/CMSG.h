/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CMSG_H
#define CMSG_H

#include "../CCommand.h"
#include "../CConnection.h"
#include "../CSession.h"
#include "../CParser.h"


class CMSG : public CCommand
{
public:
    CMSG(CSession* Session);
    virtual EParsing Run(const SMessage Message, const vector< string >& Split);
};

#endif // CMSG_H
