/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CActivatePlugin.h"

CActivatePlugin::CActivatePlugin(CSession* Session): CCommand(Session)
{

}

EParsing CActivatePlugin::Run(const SMessage Message, const std::vector< std::string, std::allocator< std::string > >& Split)
{
    string Feedback;
    if(Split.size()>1) {
        if(Session->PluginManager->LoadPlugin(Split[1])==0) {
            Session->Configuration->ActivatePlugin(Split[1]);
            Feedback="loaded "+ Split[1] + " successfully!";
            Session->Connection->sendMessage(Message.Target,Feedback);
            return EParsing::processed;
        } else {
            Feedback="could not load " + Split[1] + "!";
            Session->Connection->sendMessage(Message.Target,Feedback);
            return EParsing::infeasible;
        }
    }

    return EParsing::flawed;
}

