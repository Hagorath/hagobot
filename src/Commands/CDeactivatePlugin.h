/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CDEACTIVATEPLUGIN_H
#define CDEACTIVATEPLUGIN_H

#include "../CCommand.h"
#include "../CSession.h"
#include "../CParser.h"

class CDeactivatePlugin : public CCommand
{

public:
    CDeactivatePlugin(CSession* Session);
    virtual EParsing Run(const SMessage Message, const std::vector< std::string, std::allocator< std::string > >& Split);
};

#endif // CDEACTIVATEPLUGIN_H
