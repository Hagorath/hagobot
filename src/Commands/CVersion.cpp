/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CVersion.h"

CVersion::CVersion(CSession* Session): CCommand(Session)
{

}


EParsing CVersion::Run(const SMessage Message, const vector< string >& Split)
{
    string sending="Bot Version: "+Session->Version;
    Session->Connection->sendMessage(Message.Target,sending);
    return EParsing::processed;
}
