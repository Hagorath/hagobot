/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CNick.h"

CNick::CNick(CSession* Session): CCommand(Session)
{
    permanent=false;
}

EParsing CNick::Run(const SMessage Message, const vector< string >& Split)
{
    if(Message.Target.compare("Callback")==0) {
        if(permanent) {
            permanent=false;
            if(Session->Configuration->ChangeNick(Session->Connection->Server.Name,LastNick)==0) {
                Session->Connection->sendMessage(OldTarget,"Saved new nickname into settings file.");
            }
            return EParsing::processed;
        }
    } else if(Split.size()==2) {
        Session->Connection->ChangeNickname(Split[1]);
        Session->Connection->DIRTY_NICK_SAVE=true;
        return EParsing::onhold;
    }
    else if(Split.size()==3 && (Split[2].compare("permanent")==0)) {
        Session->Connection->ChangeNickname(Split[1]);
        LastNick=Split[1];
        permanent=true;
        OldTarget=Message.Target;
        Session->Connection->DIRTY_NICK_SAVE=true;
        return EParsing::onhold;
    }
    return EParsing::infeasible;
}
