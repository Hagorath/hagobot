/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CCommandToggle.h"


CCommandToggle::CCommandToggle(CSession* Session): CCommand(Session)
{

}

EParsing CCommandToggle::Run(const SMessage Message, const vector< string >& Split)
{
    string Result;
    if(Session->Parser->ToggleReactOnName()) {
        Result="Switched to name reaction: you should use \"";
        Result+=Session->Connection->getSelf().Nick;
        Result+=":";
        Result+=" [COMMAND] *param*\" now!";
    }
    else
    {
        Result="Switched to command reaction: you should use \"";
        Result+=Session->Parser->getStdCommand();
        Result+="[COMMAND] *param*\" now!";
    }
    Session->Connection->sendMessage(Message.Target,Result);
    return EParsing::processed;
    return EParsing::denied;
}
