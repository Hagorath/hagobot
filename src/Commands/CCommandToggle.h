/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CCOMMANDTOGGLE_H
#define CCOMMANDTOGGLE_H

#include "../CCommand.h"
#include "../CSession.h"
#include "../CParser.h"


class CCommandToggle : public CCommand
{
public:
    CCommandToggle(CSession* Session);
    virtual EParsing Run(const SMessage Message, const vector< string >& Split);
};

#endif // CCOMMANDTOGGLE_H
