/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CIdentify.h"

CIdentify::CIdentify(CSession* Session): CCommand(Session)
{

}

EParsing CIdentify::Run(const SMessage Message, const vector< string >& Split)
{
    if((Message.senderNickname.compare(Session->Admin.Nick) == 0) && (Message.Value.compare(Session->Admin.Password) == 0))
    {

        return EParsing::processed;
    } else return EParsing::denied;
}
