/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CMSG.h"

CMSG::CMSG(CSession* Session): CCommand(Session)
{

}

EParsing CMSG::Run(const SMessage Message, const vector< string >& Split)
{
    string Result;
    if(Split.size()>2) {
        Result=Message.Value.substr(1+Session->Parser->getCurCommand().size()+Split[0].size()+Split[1].size());
        Session->Connection->sendMessage(Split[1],Result);
        return EParsing::processed;
    }
    return EParsing::flawed;
}
