/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CJoin.h"

CJoin::CJoin(CSession* Session): CCommand(Session)
{

}


EParsing CJoin::Run(const SMessage Message, const vector< string >& Split)
{
    if(Split.size()>=2 && Split[1][0]=='#') {
        Session->Connection->JoinChannel(Split[1],Split.size()>2?Split[2]:"");
        return EParsing::processed;
    } else {
        return EParsing::flawed;
    }
    return EParsing::denied;
}
