/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef SSERVER_H
#define SSERVER_H
#include "default.h"

const int STD_PORT = 6667;

struct SServer
{
    string Name;
    string Hostname;
    int Port;
    string Password;
    string nickName;
    string userName;
    string realName;
    string stdChannel;

    string toString();

    bool Validation();
    bool Filled();
    SServer();
    SServer( string serverName, string serverHostname, int serverPort=STD_PORT, string nickName="HagoBot", string serverPassword="", string userName="HagoBot", string realName="HagoBot", string stdChannel="#srhbot");

    virtual ~SServer();
};

#endif // SSERVER_H
