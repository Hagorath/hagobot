/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CCONFIG_H
#define CCONFIG_H
#include "default.h"
#include <libconfig.h++>
#include <list>
#include <vector>
#include "SServer.h"

#define CONFIGURATION_FILE "./settings.cfg"
#define CONFIGURATION_MAIN "MainSettings"
#define CONFIGURATION_APLUGINS "ActivatedPlugins"

const string CONFIGURATION_SERVER = "Server";

using namespace libconfig;
class CConfig : CBase
{
    FILE *Settings;
    Config cfg;
    Setting *Root;
    Setting *ActivatedPlugins;
    Setting *Main;
    int Save();
    void Init();
    string NameLoop(string Name="NoName");
    string MakeServerName(string Name="NoName");
    string RemoveServerPre(string Name="NoName");
    string getStdServer();
public:
    int ChangeNick(string Server, string Name);
    string GetPluginDir();
    string GetStdCommand();
    bool GetNameReaction();
    vector <string> GetActivatedPlugins();
    int ActivatePlugin(string Plugin);
    int DeactivatePlugin(string Plugin);
    int addServer(string serverName = "", const string serverHostname = "", const int serverPort = 0, const string serverPassword = "", const string nickName = "", const string userName = "", const string realName = "", const string stdChannel = "", bool def = true);
    SServer getServer(string serverName="");
    pair<string,string> getAdmin();
    int addServer(const SServer& Server, bool def = true);
    int ChangeServer(const string serverName, const string serverHostname, const int serverPort, const string serverPassword, const string nickName, const string userName, const string realName, const string stdChannel="", bool def = false);
    int setDefault(const SServer &Server);
    CConfig(CSession* Session);
    virtual ~CConfig();
};

#endif // CCONFIG_H
//
