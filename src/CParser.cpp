/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CParser.h"
#include "CSession.h"
CParser::CParser(CSession* Session) : CBase(Session)
{
    StdCommand=Session->Configuration->GetStdCommand();
    Command=StdCommand;
    namereaction=Session->Configuration->GetNameReaction();
    saveCommand("kick",new CKick(Session));
    saveCommand("version",new CVersion(Session));
    saveCommand("join",new CJoin(Session));
    saveCommand("quit",new CQuit(Session));
    saveCommand("nick",new CNick(Session));
    saveCommand("cmdtoggle",new CCommandToggle(Session));
    saveCommand("cmdval",new CCommandValue(Session));
    saveCommand("msg",new CMSG(Session));
    saveCommand("loadplugin",new CActivatePlugin(Session));
    saveCommand("unloadplugin",new CDeactivatePlugin(Session));
}
void CParser::setCommandLauncher ( string String )
{
    if(String.empty())
        return;

    StdCommand=String;

    if(!namereaction)
        Command=String;
}

EParsing CParser::Parse ( SMessage Message )
{
    EParsing Result=EParsing::none;
    if(Message.Value.substr(0,Command.length()).compare(Command)==0) {
        //printf("%s erkannt!",Message.substr(0,Command.length()-1).compare(Command));
        Result|=EParsing::identified;
        Message.Value=Message.Value.substr(Command.length());
        CCommand *Command = nullptr;
        string CommandString(Message.Value);
        transform(CommandString.begin(),CommandString.end(),CommandString.begin(), ::tolower);
        Command=GetCommand(CommandString);

        if(Command==nullptr) {
            Result|=EParsing::unrecognized;
        } else {
            Result |= Command->Run(Message,SplitString(Message.Value));
        }
    } else {
        Result|=EParsing::nocommand;
    }

    return Result;
}

bool CParser::ToggleReactOnName()
{
    if(namereaction) {
        namereaction=false;
        setNameReaction(false);
        return false;
    }
    else {
        setNameReaction(true);
        namereaction=true;
        return true;
    }
}

void CParser::setNameReaction(bool val)
{
    if(val) {
        Command=Session->Connection->getSelf().Nick;
        Command+=":";
        namereaction=true;
    }
    else {
        Command=StdCommand;
        namereaction=false;
    }
}


string CParser::getStdCommand()
{
    return StdCommand;
}
string CParser::getCurCommand()
{
    return Command;
}


bool CParser::saveCommand(const char* CommandName, CCommand* Command)
{
    const string Name=CommandName;
    Commandmap.insert( pair<string,CCommand *>(Name, Command));
    return true;
    //return .second;
}

void CParser::deleteCommand(const string& CommandName)
{
    map<string,CCommand *>::iterator iter=Commandmap.find(CommandName);
    if(iter!=Commandmap.end())
    {
        delete iter->second;
        Commandmap.erase(iter);
    }
}

CCommand* CParser::GetCommand(const string& value)
{
    //printf("splitting message!\n");
    vector<string>Strings=SplitString(value);
    //printf("finding command!\n");
    if(!Strings.empty()) {
        for(map<string, CCommand *>::iterator iter = Commandmap.begin(); iter!=Commandmap.end(); iter++) {
            if(Strings[0].compare(iter->first)==0) {
                printf("Command found!\n");
                return iter->second;
            }
        }
    }
    return nullptr;
}

bool CParser::getNameReaction()
{
    return namereaction;
}


vector< string > CParser::SplitString(string value)
{
    vector<string> Result;
    stringstream Stream;
    for(string::iterator iter=value.begin(); iter!=value.end(); ++iter) {
        if(*iter != ' ' && *iter != '\t') {
            Stream << *iter;
        } else {
            if(!Stream.str().empty())
                Result.push_back(Stream.str());
            Stream.str("");
            Stream.clear();
        }
    }
    Result.push_back(Stream.str());
    Stream.clear();
    return Result;
}

CParser::~CParser()
{

}
