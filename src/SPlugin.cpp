/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "SPlugin.h"

SPlugin::SPlugin(CSession* Session) : CBase(Session)
{

}


SPlugin::~SPlugin()
{

}


void SPlugin::onEventTrigger(const SMessage& Message, const EEvent Event)
{

}
