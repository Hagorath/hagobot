/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CCHANNEL_H
#define CCHANNEL_H
#include "default.h"
#include "SUser.h"
#include <map>
struct SChannel : public map<string, SUser>
{
    string Name;
    string Password;
    string Notice;
    SChannel(const map< string, SUser >& __x);
    SChannel();
    SChannel(map< string, SUser >& __x);
};

#endif // CCHANNEL_H
