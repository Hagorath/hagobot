
#ifndef CEVPLUGIN_H
#define CEVPLUGIN_H
#include "SMessage.h"
#include "SPlugin.h"
#include <map>


class CSession;
struct IEventHandler
{

    typedef std::multimap<EEvent, SPlugin*> PluginMap;

    /**
     *Ineffizient, aber keine Lust mehr :)
     *
     *
     * */
    PluginMap EventMap;
    virtual void Registry(EEvent, SPlugin*);
    virtual void Depart(EEvent, SPlugin*);
    virtual void onEvent(const SMessage &Message, const std::string& Arg2, const EEvent Event) {};

    //std::map<std::string,(SPlugin::*onDisconnect)(void)> DisconnectM;
//   std::map<std::string,(SPlugin::*onConnect)(void)> ConnectM;
//   std::map<std::string,(SPlugin::*onMessage)(SMessage)>MessageM;
//   std::map<std::string,(SPlugin::*onNotice)(SMessage)> NoticeM;
//   std::map<std::string,(SPlugin::*onNicknameChange)(SMessage)> NicknameChangeM;
//   std::map<std::string,(SPlugin::*onRawLine)(const std::string&)> RawLineM;
//   std::map<std::string,(SPlugin::*onCtcpRequest)(SMessage)> CtcpRequestM;
//   std::map<std::string,(SPlugin::*onCtcpResponse)(SMessage)> CtCpResonseM;
//   std::map<std::string,(SPlugin::*onAction)(SMessage)> ActionM;
//   std::map<std::string,(SPlugin::*onJoinChannel)(SMessage)> JoinChannelM;
//   std::map<std::string,(SPlugin::*onPartChannel)(SMessage)> PartChannelM;
//   std::map<std::string,(SPlugin::*onKick)(const std::string&, SMessage)> KickM;
//     typedef std::map<std::string,std::function<void()>> FunctionMapVoid;
//     typedef std::map<std::string,std::function<void(const std::string&)>> FunctionMapRaw;
//     typedef std::map<std::string,std::function<void(SMessage)>> FunctionMapMSG;
//     typedef std::map<std::string,std::function<void(const std::string&, SMessage)>> FunctionMapExtra;
//     typedef std::map<std::string,std::function<void(const std::string&, SMessage)>> FunctionMapString;
//     FunctionMapVoid DisconnectM;
//     FunctionMapVoid ConnectM;
//     FunctionMapMSG MessageM;
//     FunctionMapMSG NoticeM;
//     FunctionMapMSG NicknameChangeM;
//     FunctionMapRaw RawLineM;
//     FunctionMapMSG CtcpRequestM;
//     FunctionMapMSG CtCpResonseM;
//     FunctionMapMSG ActionM;
//     FunctionMapMSG JoinChannelM;
//     FunctionMapMSG PartChannelM;
//     FunctionMapExtra KickM;
};

#endif // CEVPLUGIN_H
