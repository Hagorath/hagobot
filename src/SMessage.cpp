/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "SMessage.h"

SMessage::SMessage(string Target, string senderNickname, string Value, string senderUsername, string senderHostname, string reason, EMessageFlag Flag)
{
    this->Flag  =Flag;
    this->Target=Target;
    this->senderNickname = senderNickname;
    this->senderHostname= senderHostname;
    this->senderUsername= senderUsername;
    this->Reason=reason;
    this->Value = Value;
}
