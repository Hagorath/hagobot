/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef SPLUGIN_H
#define SPLUGIN_H

#include "CBase.h"
#include "SMessage.h"
#include <string>


struct SPlugin : public CBase
{
    typedef SPlugin* construct_plugin_t(CSession * Session);
    typedef void destruct_plugin_t(SPlugin *);

    SPlugin(CSession* Session);
    std::string Name;
    virtual void Start() {};
    virtual void Init() {};
    virtual void Stop() {};
    virtual void onEventTrigger(const SMessage& Message, const EEvent Event);
    ////////////Hooks
//     virtual void onDisconnect() {}
//     virtual void onConnect() {}
//     virtual void onMessage(SMessage Message) {}
//     virtual void onNotice(SMessage Message) {}
//     virtual void onNicknameChange (SMessage Massage) {}
//     virtual void onRawLine ( const string& line ) {}
//     virtual void onCtcpRequest ( SMessage Massage ) {}
//     virtual void onCtcpResponse ( SMessage Massage ) {}
//     virtual void onAction ( SMessage Massage ) {}
//     virtual void onJoinChannel ( SMessage Massage ) {}
//     virtual void onPartChannel ( SMessage Massage ) {}
//     virtual void onKick ( const std::string& kickedNickname, SMessage Massage ) {}
    virtual ~SPlugin();
};

typedef std::pair<SPlugin*,SPlugin::destruct_plugin_t *> SPlugin_t;
#endif // SPLUGIN_H
