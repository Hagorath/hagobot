/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CDatabase.h"
#include "CmdLastseen.h"
#include "CmdDumpHistory.h"
#include "CmdShowLog.h"
#include "CmdSaveHistory.h"

extern "C" CDatabase* construct(CSession * Session) {
    return new CDatabase(Session);
}
extern "C" void destruct(CDatabase *p) {
    delete p;
}

CDatabase::CDatabase(CSession* Session): SPlugin(Session)
{
    db=nullptr;
    Qu='\'';
    BQu="\',\'";

}

void CDatabase::Init()
{
    Session->Parser->saveCommand("lastseen",new CmdLastseen(Session,this));
    Session->Parser->saveCommand("dumphistory",new CmdDumpHistory(Session,this));
    Session->Parser->saveCommand("showlog",new CmdShowLog(Session,this));
    Session->Parser->saveCommand("savehistory",new CmdSaveHistory(Session,this));

    Session->Connection->Registry(EEvent::message,this);
    Session->Connection->Registry(EEvent::kick,this);
    Session->Connection->Registry(EEvent::joinchannel,this);
    Session->Connection->Registry(EEvent::action,this);
    Session->Connection->Registry(EEvent::nickchange,this);
    Session->Connection->Registry(EEvent::partchannel,this);
    Session->Connection->Registry(EEvent::notice,this);
}


void CDatabase::Start()
{
    if(Open()==0)
        printf("Opened Database!\n");
    else
        Session->PluginManager->UnloadPlugin("db.so");
}


int CDatabase::Open()
{
    if ( ( Open_ ("history.db" ) != SQLITE_OK ) ) {
        printf ( "Something is wrong here!\n" );
        return 1;
    } else if ( ( CreateTable() == 0 ) ) {
        printf("created history table.\n");
        return 0;
    }
    else return 1;
}


sqlite3_stmt* CDatabase::sqlite3_prepare ( string Query )
{
    if ( db==nullptr ) {
        return nullptr;
    }
    sqlite3_stmt *statement = nullptr;
    if ( ( ::sqlite3_prepare_v2 ( db, Query.c_str(), -1, &statement, 0 ) ) != SQLITE_OK ) {
        printf ( "Query fehlerhaft:\n   %s,\n   %s\n\n", Query.c_str(),sqlite3_errmsg ( db ) );
        return nullptr;
    }

    return statement;
}


int CDatabase::Open_ ( const char* databasefile )
{
    int status = sqlite3_open_v2 ( databasefile, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL );

    if ( status != SQLITE_OK ) {
        printf ( "Couldn't open Database'!\n" );
        throw string ( sqlite3_errmsg ( db ) );
    } else if ( status == SQLITE_OK )
        return 0;
    else return -1;
}



int CDatabase::CreateTable()
{
    //sqlite3_stmt * stmt=nullptr;
    int i=sqlite3_exec ( "CREATE TABLE IF NOT EXISTS history"
                         "(date INTEGER, type INTEGER, target TEXT, nick TEXT, user TEXT, host TEXT, message TEXT, reason TEXT, server TEXT);" );
    if(i != SQLITE_OK ) {
        printf("Error in making Table!\n");
    }
    else {
//       sqlite3_step(stmt);
//       sqlite3_finalize(stmt);
        printf("Created Database!\n");
    }
    return 0;
}




int CDatabase::sqlite3_exec(const string& Query)
{
    return ::sqlite3_exec(db, Query.c_str(), NULL ,NULL,NULL);
}


int CDatabase::removeFromLog()
{
    return 0;
}

void CDatabase::Close()
{
    if(db!=nullptr)
        sqlite3_close ( db );
}




CDatabase::~CDatabase()
{
    Session->Parser->deleteCommand("lastseen");
    Session->Parser->deleteCommand("dumphistory");
    Session->Parser->deleteCommand("showlog");
    Session->Parser->deleteCommand("savehistory");

    Session->Connection->Depart(EEvent::message,this);
    Session->Connection->Depart(EEvent::kick,this);
    Session->Connection->Depart(EEvent::joinchannel,this);
    Session->Connection->Depart(EEvent::action,this);
    Session->Connection->Depart(EEvent::nickchange,this);
    Session->Connection->Depart(EEvent::partchannel,this);
    Session->Connection->Depart(EEvent::notice,this);
    Close();
}

void CDatabase::onEventTrigger(const SMessage& Message, const EEvent Event)
{
    SContainer Container(Session->getCurrentTime(),Session->Connection->Server.Hostname ,Message);
    if(Event==EEvent::message || Event==EEvent::kick || Event==EEvent::joinchannel || Event==EEvent::action ||
            Event==EEvent::nickchange || Event==EEvent::partchannel || Event==EEvent::notice )
        saveEntry(Container);
    sqlite3_step()
    else return;
}

sqlite3_stmt* CDatabase::saveEntry(SContainer Container)
{
    std::string Query="INSERT INTO history (type, target, nick, user, host, message, reason, server) VALUES ("+IntToStr((int)Container.Message.Flag)+",\'";
    Query+=Container.Message.Target+BQu;
    Query+=Container.Message.senderNickname+BQu;
    Query+=Container.Message.senderUsername+BQu;
    Query+=Container.Message.senderHostname+BQu;
    Query+=Container.Message.Value+BQu;
    Query+=Container.Message.Reason+BQu;
    Query+=Container.Server+Qu+");";
    sqlite3_stmt* stmt=sqlite3_prepare(Query);

    sqlite3_bind_int64(stmt, 1, Container.time);
    sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    return stmt;
}

string CDatabase::chToStr(const unsigned char* cstring )
{
    stringstream Stream;
    Stream << cstring;
    return Stream.str();
}


SContainer CDatabase::LookupLastseen(string Nickname)
{
    SContainer Container;
    std::string Query="SELECT * FROM history WHERE nick=\'"+Nickname+"\' OR (message=\'"+Nickname+"\' AND"
                      " (type="+to_string((int)EMessageFlag::kick)+" OR type="+to_string((int)EMessageFlag::nickchange)+")) ORDER BY date DESC LIMIT 1;";

    sqlite3_stmt* stmt =sqlite3_prepare(Query);
    if(stmt!=nullptr)
    {
        int Status = sqlite3_step(stmt);
        if(Status== SQLITE_ROW) {
            printf("ETWAS GEFUNDEN!\n");
            Container.time=(time_t)sqlite3_column_int64(stmt,1);
            Container.Message.Flag=(EMessageFlag)sqlite3_column_int(stmt,1);
            Container.Message.Target=chToStr(sqlite3_column_text(stmt,2));
            Container.Message.senderNickname=chToStr(sqlite3_column_text(stmt,3));
            Container.Message.senderUsername=chToStr(sqlite3_column_text(stmt,4));
            Container.Message.senderHostname=chToStr(sqlite3_column_text(stmt,5));
            Container.Message.Value=chToStr(sqlite3_column_text(stmt,6));
            Container.Message.Reason=chToStr(sqlite3_column_text(stmt,7));
            Container.Server=chToStr(sqlite3_column_text(stmt,8));
        }
        sqlite3_finalize(stmt);
    }
    return Container;
}

void CDatabase::dumpHistory()
{
    sqlite3_exec("DROP TABLE history;");
    CreateTable();
}

vector< SContainer > CDatabase::showLog(string from, string to)
{
    vector<SContainer> Log;

    std::string Query="SELECT * FROM history WHERE ID>=";
    Query+=from;
    Query+=" and ID<=";
    Query+=to;
    Query+=";";

    sqlite3_stmt* stmt =sqlite3_prepare(Query);
    while(sqlite3_step(stmt) ==SQLITE_ROW) {
        SContainer Container;
        Container.time=(time_t)sqlite3_column_int64(stmt,1);
        Container.Message.Flag=(EMessageFlag)sqlite3_column_int(stmt,1);
        Container.Message.Target=chToStr(sqlite3_column_text(stmt,2));
        Container.Message.senderNickname=chToStr(sqlite3_column_text(stmt,3));
        Container.Message.senderUsername=chToStr(sqlite3_column_text(stmt,4));
        Container.Message.senderHostname=chToStr(sqlite3_column_text(stmt,5));
        Container.Message.Value=chToStr(sqlite3_column_text(stmt,6));
        Container.Message.Reason=chToStr(sqlite3_column_text(stmt,7));
        Container.Server=chToStr(sqlite3_column_text(stmt,8));
        Log.push_back(Container);
    }
    sqlite3_finalize(stmt);
    return Log;
}

int CDatabase::getMaxID()
{
    std::string Query="SELECT MAX(ROWID) from history;";
    int i=0;
    sqlite3_stmt* stmt =sqlite3_prepare(Query);
    if(sqlite3_step(stmt) ==SQLITE_ROW) {
        i=sqlite3_column_int(stmt,0);
    }
    sqlite3_finalize(stmt);
    return i;
}
string CDatabase::IntToStr(int integer)
{
    std::ostringstream convert;
    convert<<integer;
    return convert.str();

}
