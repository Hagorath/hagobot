/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CmdDumpHistory.h"

CmdDumpHistory::CmdDumpHistory(CSession* Session, CDatabase* Database): CCommand(Session), Database(Database)
{

}

EParsing CmdDumpHistory::Run(const SMessage Message, const std::vector< std::string, std::allocator< std::string > >& Split)
{
    Database->dumpHistory();
    Session->Connection->sendMessage(Message.Target,"History dumped!");
    return EParsing::processed;
}

