/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CmdLastseen.h"

CmdLastseen::CmdLastseen(CSession* Session, CDatabase* Database): CCommand(Session), Database(Database)
{
    Session=Session;
}

EParsing CmdLastseen::Run(const SMessage Message, const vector< string >& Split)
{
    string Result;
    SContainer Container;
//     for(uint i=1; i<=Split.size(); i++) {
    if(Split.size()==1) {
        Container=  Database->LookupLastseen(Message.senderNickname);
    } else if(Split.size()==2) {
        Container=  Database->LookupLastseen(Split[1]);
    } else  return EParsing::none;

    switch(Container.Message.Flag) {
    case EMessageFlag::action:
        Result="nothing to comment:";
        break;
    case EMessageFlag::join:
        Result="joined at and then i was gone...";
        break;
    case EMessageFlag::kick:
        Result="got kicked by ";
        break;
    case EMessageFlag::leave:
        Result="left at";
        break;
    case EMessageFlag::message:
        Result="written and then i didn\'t care...";
        break;
    case EMessageFlag::nickchange:
        Result="Changed his nick to ";
        break;
    case EMessageFlag::notice:
        Result="..made a notice about ";
        break;
    case EMessageFlag::none:
    default:
        Result="never met that person...";
        break;
    }
    Session->Connection->sendMessage(Message.Target,Result);
    //Session->Connection->sendMessage(Message.Target,Result);
//     }
    return EParsing::processed;
}
