/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CMDLASTSEEN_H
#define CMDLASTSEEN_H

#include "../src/ETypes.h"
#include "../src/CBase.h"
#include "../src/default.h"
#include "../src/SMessage.h"
#include "../src/SPlugin.h"
#include "../src/CSession.h"
#include "../src/CCommand.h"
#include "CDatabase.h"
#include "SContainer.h"
#include <cstdio>


class CmdLastseen : public CCommand
{
    CDatabase* Database;
public:
    CmdLastseen(CSession* Session, CDatabase* Database);
    virtual EParsing Run(const SMessage Message, const vector< string >& Split);
};

#endif // CMDLASTSEEN_H
