/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CmdSaveHistory.h"

CmdSaveHistory::CmdSaveHistory(CSession* Session, CDatabase* Database): CCommand(Session), Database(Database)
{

}

EParsing CmdSaveHistory::Run(const SMessage Message, const vector< string >& Split)
{
    Session->Connection->sendMessage(Message.Target,"processing...");
    Database->Close();
    Database->Open();
    Session->Connection->sendMessage(Message.Target,"Saved history.");
    return EParsing::processed;
}
