/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#include "CmdShowLog.h"

CmdShowLog::CmdShowLog(CSession* Session, CDatabase* Database): CCommand(Session), Database(Database)
{

}

EParsing CmdShowLog::Run(const SMessage Message, const vector< string >& Split)
{
    string Result;

    if(Database->getMaxID()!=0) {
        Result="There are about ";
        Result+=Database->getMaxID();
        Result+=" entries.";

        vector< SContainer > ContainerVector;
        if(Split.size()==1) {
            int i=Database->getMaxID();
            std::ostringstream convert;
            convert << i-10;
            string from=convert.str();
            convert.clear();
            convert << i-10;
            string to=convert.str();
            ContainerVector=Database->showLog(from,to);
        } else if(Split.size()==2) {
            int i=Database->getMaxID();
            std::ostringstream convert;
            convert << i;
            ContainerVector=Database->showLog(Split[1].c_str(),convert.str());
        } else if(Split.size()==3) {
            ContainerVector=Database->showLog(Split[1].c_str(),Split[2].c_str());
        }

        Session->Connection->sendMessage(Message.Target,Result);
        Result="";
        for(vector < SContainer>::iterator iter= ContainerVector.begin(); iter!=ContainerVector.end(); iter++) {
            Result=Session->TimeString(iter->time,"<%F %R>");
            Result+=iter->Message.senderNickname+"  ";
            switch(iter->Message.Flag) {
            case EMessageFlag::action:
                Result+="<";
                Result+=iter->Message.Value;
                Result+=">";
                break;
            case EMessageFlag::join:
                Result+="joined on ";
                Result+=iter->Message.Target;
                Result+=".";
                break;
            case EMessageFlag::kick:
                Result+="has kicked ";
                Result+=iter->Message.Value;
                Result+=" from ";
                Result+=iter->Message.Target;
                Result+=" with this reason: ";
                Result+=iter->Message.Reason;
                break;
            case EMessageFlag::leave:
                Result+="has left ";
                Result+=iter->Message.Target+".";
                break;
            case EMessageFlag::message:
                Result+=": ";
                Result+=iter->Message.Value;
                break;
            case EMessageFlag::nickchange:
                Result+=" changed nick from ";
                Result+=iter->Message.Value;
                Result+=".";
                break;
            case EMessageFlag::notice:
                Result+= "made a notice: ";
                Result+= iter->Message.Value;
                Result+= " in ";
                Result+= iter->Message.Target;
                break;
            default:
            case EMessageFlag::none:
                break;
            }
        }
    } else {
        Result="Nothing to tell... \n";
    }
    Session->Connection->sendMessage(Message.Target,Result);
    return EParsing::flawed;
}
