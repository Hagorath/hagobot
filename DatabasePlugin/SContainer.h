/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef SCONTAINER_H
#define SCONTAINER_H

#include "../src/ETypes.h"
#include "../src/CBase.h"
#include "../src/default.h"
#include "../src/SMessage.h"
#include "../src/SPlugin.h"
#include "../src/CSession.h"

struct SContainer
{
    time_t time;
    std::string Server;
    SMessage Message;
    SContainer(time_t time=0,std::string Server="", SMessage Message=SMessage())
        : time(time), Server(Server), Message(Message) {}
};

#endif // SCONTAINER_H
