/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CDATABASE_H
#define CDATABASE_H
#include <sqlite3.h>
#include <string>
#include "../src/ETypes.h"
#include "../src/CBase.h"
#include "../src/default.h"
#include "../src/SMessage.h"
#include "../src/SPlugin.h"
#include "../src/CSession.h"
#include "SContainer.h"
#include <cstdio>

class CDatabase : SPlugin
{
    sqlite3 *db;
    int Open_ (const char* databasefile );

    sqlite3_stmt* sqlite3_prepare (string Query );
    int sqlite3_exec(const string& Query);
    int CreateTable();
    char Qu;
    std::string BQu;
    string chToStr(const unsigned char* cstring);
    string IntToStr(int integer);
public:
    int Open();
    void Close();
    CDatabase(CSession* Session);
    virtual ~CDatabase();
    virtual void Start();
    virtual void Init();
    int removeFromLog();
    vector<SContainer> showLog ( string from, string to );
    int getMaxID();
    void dumpHistory();
    sqlite3_stmt* saveEntry(SContainer Container);
    SContainer LookupLastseen(string Nickname);
    virtual void onEventTrigger(const SMessage& Message, const EEvent Event);
};


#endif // CDATABASE_H

