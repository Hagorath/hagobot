/*
* ----------------------------------------------------------------------------
* "THE PIZZA-WARE LICENSE" (Revision 42):
* <tassilo.volk@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a pizza in return Tassilo Volk
* ----------------------------------------------------------------------------
*/

#ifndef CMDSAVEHISTORY_H
#define CMDSAVEHISTORY_H

#include "../src/ETypes.h"
#include "../src/CBase.h"
#include "../src/default.h"
#include "../src/SMessage.h"
#include "../src/SPlugin.h"
#include "../src/CSession.h"
#include "../src/CCommand.h"
#include "CDatabase.h"
#include "SContainer.h"
#include <cstdio>


class CmdSaveHistory : public CCommand
{
    CDatabase* Database;
public:
    CmdSaveHistory(CSession* Session, CDatabase* Database);
    virtual EParsing Run(const SMessage Message, const vector< string >& Split);
};

#endif // CMDSAVEHISTORY_H
